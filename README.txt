Collects news articles from Le Monde, Le Figaro and Les Echos.

# Download spacy fr model
spacy download fr

# Select relevant topics
python main.py --extract_topics_from /data/tcao/news-please-repo/data/2018/02/09/lemonde.fr/,/data/tcao/news/lemondefr*/ --domain_name lemonde.fr> topics_lemonde.txt
python main.py --extract_topics_from /data/tcao/news-please-repo/data/2018/02/09/lefigaro.fr/,/data/tcao/news/Le_Figaro*/ --domain_name lefigaro.fr > topics_lefigaro.txt
python main.py --topics /data/tcao/news/lesechos2017 --topics_file lesechos_topics2017.txt --doc_topics_file lesechos_doc_topics2017.txt --doc_ids_file lesechos_doc_id2017.txt

python main.py --topics "/data/tcao/news/lesechos*" --topics_file lesechos_topics_all.txt --doc_topics_file lesechos_doc_topics_all.txt --doc_ids_file lesechos_doc_id_all.txt

# Select articles from relevant topics
python main.py --list_articles=/data/tcao/news-please-repo/data/2018/02/09/lemonde.fr/,/data/tcao/news/lemondefr*/ \
    --selected_topics=economie,economie-francaise,emploi \
    --selected_articles_file=selected_articles_lemonde.txt
python main.py --list_articles=/data/tcao/news-please-repo/data/2018/02/09/lefigaro.fr/,/data/tcao/news/Le_Figaro*/ \
    --selected_topics=flash-eco,economie,emploi \
    --selected_articles_file=selected_articles_lefigaro.txt

python main.py --selected_topic_ids=1,2,3,7 \
    --doc_topics_file=lesechos_doc_topics_all.txt \
    --doc_ids_file=lesechos_doc_id_all.txt \
    --selected_articles_file=selected_articles_lesechos.txt

# Select the articles to build gold standard sentences
python main.py --select_gold_standard="selected*.txt" --num_docs=50 --selected_articles_file=gold_standard_articles.txt

# Collect the extracted HTML from all articles
python main.py --collect_html=/data/tcao/news-please-repo/data/2018/02/09/lemonde.fr/,/data/tcao/news-please-repo/data/2018/02/09/lefigaro.fr/,/data/tcao/news/lemondefr*/,/data/tcao/news/Le_Figaro*/,/data/tcao/news/lesechos* --all_urls_file=all_urls.txt

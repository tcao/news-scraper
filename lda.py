from gensim import corpora
from gensim.models.ldamodel import LdaModel
from gensim.models import Word2Vec
from gensim.models.phrases import Phraser
from nltk.corpus import stopwords
import spacy
from operator import itemgetter


lang = 'fr'
mincount = 50
size = 300
ngram_level = 2
threshold = 2
WORD2VEC_FOLDER = '/data/tcao'
phraser_file = WORD2VEC_FOLDER + \
        '/phraser/phraser_web{}_mincount{}_t{}_level{{}}.bin'.format(\
        lang, mincount, threshold)
phraser = Phraser.load(phraser_file.format(2))

nlp = spacy.load('fr')

stoplist = stopwords.words('french')
stoplist.extend(['»', '«', 'selon', 'les', '%', '(', ')', 'a', 'cet', 'ce', 'cette', \
        'd’', 'l’', 's’', ',', '.', '+', '-', '–', ':', "l'", "d'", '"', "qu'", '?', \
        "c'", "n'", "n’", "s'", "plus", "comme", "si", "...", "alors", "ils", "elles", "leurs", \
        "être", "dont", "donc"])


def tokenize(sentence):
    # return phraser[[token.text for token in nlp(sentence.lower())]]
    return [token.text for token in nlp(sentence.lower())]


def generate_topics(documents, num_topics, doc_topics_file, topics_file, num_words_per_topics=30):

    texts = [[word for word in tokenize(document) if word not in stoplist] \
            for document in documents]
    dictionary = corpora.Dictionary(texts)
    corpus = [dictionary.doc2bow(text) for text in texts]
    lda = LdaModel(corpus=corpus, id2word=dictionary, num_topics=num_topics, random_state=2018)
    topics = lda.show_topics(num_topics=num_topics, num_words=num_words_per_topics, formatted=False)

    with open(doc_topics_file, 'w+') as f:
        for doc_id, doc_bow in enumerate(corpus):
            doc_topics = lda.get_document_topics(doc_bow)
            topic = max(doc_topics, key=itemgetter(1))[0]
            f.write('%d, %d\n' % (doc_id, topic))
    
    with open(topics_file, 'w+') as f:
        for (topic_id, list_word_prob) in topics:
            f.write(str(topic_id) + '\n')
            for word, prob in list_word_prob:
                f.write('%s, ' % word)
            f.write('\n')

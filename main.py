import sys

import argparse
import re
import json
import utils
import os
import glob
import time
import random
from collections import Counter
from newsplease.newsplease import NewsPlease


random.seed(2018)


def extract_urls(tweets_csv_file):
    with open(tweets_csv_file) as f:
        all_urls = list()
        for line in f.read().splitlines():
            urls = re.findall(\
                    'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', line)
            all_urls.extend(urls)

    return all_urls


def wait_randomly():
    waiting_time = random.randint(0, 10)
    time.sleep(waiting_time)


def extract_article(shortened_url):
    try:
        url = utils.url_from(shortened_url)
        url = url[: url.index('?')] if '?' in url else url

        wait_randomly()

        extract_article_from_full_url(url)
    except:
        pass


def extract_article_from_full_url(url):
    try:
        print(url)
        json_file = os.path.join(folder, utils.url_to_file_name(url) + '.json')
        if os.path.exists(json_file):
            print('Skip')

        article = NewsPlease.from_url(url)
        if article:
            with open(json_file, 'w+') as f:
                dict_article = utils.article_to_dict(article)
                json.dump(dict_article, f)
    except:
        pass


def extract_url_topic(url, domain_name):
    if domain_name in url:
        tokens = url.split('/')
        if len(tokens) > 3:
            return tokens[3]
    return None


def list_folders(patterns):
    '''
    Args:
        patterns (str): folder_1,folder_2*,...
    '''
    folder_patterns = patterns.split(',')
    folders = list()
    for pattern in folder_patterns:
        folders.extend(glob.glob(pattern))
    return folders


def traverse_json_file(folders, process_url):
    url_cache = dict()
    for news_json_folder in folders:
        for json_file in os.listdir(news_json_folder):
            file_path = os.path.join(news_json_folder, json_file) 
            with open(file_path) as f:
                try:
                    article = json.load(f)
                    url = article['url']
                    if url not in url_cache:
                        url_cache[url] = True
                        process_url(url, file_path)
                except:
                    pass


def selected_gold_standard(list_selected_articles_files, num_docs, selected_articles_file):
    with open(selected_articles_file, 'w+') as f_selected:
        for selected_articles_file in list_selected_articles_files:
            with open(selected_articles_file) as f:
                files = [line.split(',')[0] for line in f.read().splitlines()]
                random.shuffle(files)
                selected_files = files[: num_docs]
                f_selected.writelines([file_path + '\n' for file_path in selected_files])


def collect_html(arg):
    wait_randomly()
    url, html_file = arg
    try:
        _, html = NewsPlease.from_url(url, return_html=True)
        print('Write html to ' + html_file)
        with open(html_file, 'w+') as f:
            f.write(html)
    except:
        pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--extract_urls_from", help="Extract URLs from a CSV file that contains tweets")
    parser.add_argument("--folder", help="Folder to store the extracted articles")

    parser.add_argument("--extract_topics_from", help="Extract topic of URLs from a list of folders of news articles in\
            JSON format, each folder is separated by a comma")
    parser.add_argument("--domain_name", help="Domain name of the newspaper that we want to extract topics")

    parser.add_argument("--topics", help="Display topics from a folder of news articles in JSON format")
    parser.add_argument("--topics_file", help="The file to store topic IDs and their keywords")
    parser.add_argument("--doc_topics_file", help="The file to store topic probabilities of each news articles")
    parser.add_argument("--doc_ids_file", help="The file to store mapping of news articles and their IDs")
    parser.add_argument("--selected_topic_ids", help="The selected topic ids separated by comma")

    parser.add_argument("--list_articles", help="Build a list of articles that belong to a list of topics")
    parser.add_argument("--selected_topics", help="The selected topics")
    parser.add_argument("--selected_articles_file", help="The file to store the selected articles")

    parser.add_argument("--select_gold_standard", help="Select the list of articles from which gold \
            standard sentences will be chosen")
    parser.add_argument("--num_docs", type=int, help="Number of articles to select per file")

    parser.add_argument("--collect_html", help="Collect HTML from articles, each folder/folder pattern is separated by a comma")
    parser.add_argument("--all_urls_file", help="The file to store the mapping of article's url, JSON file and HTML file")

    args = parser.parse_args()

    domain_name = args.domain_name

    if args.folder:
        global folder
        folder = args.folder
        if not os.path.exists(folder):
            os.makedirs(folder)

    if args.extract_urls_from:
        utils.parallel(extract_article, extract_urls(args.extract_urls_from))
    
    if args.extract_topics_from:
        folders = list_folders(args.extract_topics_from)

        counter = Counter()
        def process_url(url, json_file):
            topic = extract_url_topic(url, domain_name)
            if topic:
                counter[topic] += 1

        traverse_json_file(folders, process_url)

        for t in counter.most_common():
            print(t)

    if args.topics:
        import lda
        folders = glob.glob(args.topics)

        documents = list()
        doc_id = 0
        with open(args.doc_ids_file, 'w+') as f_doc_ids:
            for news_json_folder in folders:
                list_files = os.listdir(news_json_folder)
                for json_file in list_files:
                    file_path = os.path.join(news_json_folder, json_file) 
                    with open(file_path) as f:
                        article = json.load(f)
                    if article and article['text']:
                        text = article['text']
                        documents.append(text.replace('\n', ''))

                        f_doc_ids.write('%d, %s\n' % (doc_id, file_path))
                        doc_id += 1
                
        lda.generate_topics(documents=documents, doc_topics_file=args.doc_topics_file, \
                topics_file=args.topics_file, num_topics=20)

    if args.list_articles:
        folders = list_folders(args.list_articles)
        selected_topics = args.selected_topics.split(',')
        selected_topics = ['/' + topic + '/' for topic in selected_topics]

        with open(args.selected_articles_file, 'w+') as f:
            def process_url(url, json_file):
                for topic in selected_topics:
                    if topic in url:
                        f.write('%s, %s\n' % (json_file, url))
                        break

            traverse_json_file(folders, process_url)

    if args.selected_topic_ids:
        topic_ids = args.selected_topic_ids.split(',')

        selected_docs = dict()
        with open(args.doc_topics_file) as f:
            for line in f.read().splitlines():
                doc_id, topic = [token.strip() for token in line.split(',')]
                if topic in topic_ids:
                    selected_docs[doc_id] = True

        with open(args.selected_articles_file, 'w+') as f_selected:
            with open(args.doc_ids_file) as f:
                for line in f.read().splitlines():
                    doc_id, file_path = [token.strip() for token in line.split(',')]
                    if doc_id in selected_docs:
                        f_selected.write('%s\n' % file_path)

    if args.select_gold_standard:
        list_selected_articles_files = glob.glob(args.select_gold_standard)
        selected_gold_standard(list_selected_articles_files, args.num_docs, \
                args.selected_articles_file)

    if args.collect_html:
        folders = list_folders(args.collect_html)

        f = open(args.all_urls_file, 'w+') 
        list_arguments = list()
        def process_url(url, json_file):
            html_file = json_file.replace('.json', '.html')
            list_arguments.append((url, html_file))
            f.write('%s, %s, %s\n' % (url, json_file, html_file))

        traverse_json_file(folders, process_url)
        f.close()

        utils.parallel(collect_html, list_arguments)
        


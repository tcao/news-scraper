import multiprocessing
import requests


def parallel(function, arguments, max_processes = multiprocessing.cpu_count() - 2):
    pool = multiprocessing.Pool(processes=max_processes)
    results = pool.map(function, arguments)
    pool.close()
    pool.join()
    return results


def article_to_dict(article):
    return {
        'authors': article.authors,
        'date_download': str(article.date_download),
        'date_modify': str(article.date_modify),
        'date_publish': str(article.date_publish),
        'description': article.description,
        'image_url': article.image_url,
        'source_domain': article.source_domain,
        'text': article.text,
        'title': article.title,
        'title_page': article.title_page,
        'title_rss': article.title_rss,
        'url': article.url
    }


def url_from(shortened_url):
    session = requests.Session()
    resp = session.head(shortened_url, allow_redirects=True)
    return resp.url


def url_to_file_name(url):
    file_name = url.replace('http://', '').replace('https://', '').replace('.', '-').replace('/', '-')
    return file_name[: file_name.index('?')] if '?' in file_name else file_name
